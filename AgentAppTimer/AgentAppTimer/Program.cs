using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using System.Net;

namespace AgentAppTimer
{
    class Program
    {
        static void Main(string[] args)
        {
           
            JobScheduler jobScheduler = new JobScheduler();
            jobScheduler.Start();
            Console.ReadLine();
        }

        public class JobScheduler
        {
            public async void Start()
            {
                try
                {
                    ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
                    IScheduler scheduler = await schedulerFactory.GetScheduler();
                    await scheduler.Start();

                    IJobDetail job = JobBuilder.Create<HelloJob>().Build();

                    ITrigger trigger = TriggerBuilder.Create()

                        .WithIdentity("HelloJob", "GreetingGroup")

                        .WithCronSchedule("0 0 * ? * * *") //execute every hour
                        //.WithCronSchedule("* * * ? * *")  //execute every sec

                        .StartAt(DateTime.UtcNow)

                        .WithPriority(1)

                        .Build();

                    await scheduler.ScheduleJob(job, trigger);
                  
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                   WriteLog.Write("ERROR: " + ex.Message.ToString());
                  
                }
            }
        }
        public class HelloJob : IJob
        {
         Task IJob.Execute(IJobExecutionContext context)
            {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }
        Task taskA = new Task(() => {
           
            //get the method that schedule if is end of the month
                var url = ConfigurationManager.AppSettings["apiems"];

            // adds to GET or URL querystring based on Method
            var client = new RestClient(
                
                url);

            var softrequest = new RestRequest("v1/AgentAppTimer/CreateTimer", Method.GET);

            IRestResponse softresponse = client.Execute(softrequest);

            if (softresponse.StatusCode != HttpStatusCode.OK)
            {

                WriteLog.Write(string.Format("{0} Returned, Web service is down \n",
                  url, softresponse.StatusDescription));
            }
        }
         
        
        );
          taskA.Start();               

                
        return taskA;
    }
}

    }
}
