﻿using EMS.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Net.Http;
using System.Net;
using System.Configuration;
using System.Collections.Specialized;
using System.Windows.Threading;
using System.Net.NetworkInformation;

namespace AgentDeviceApp.Classes
{
    class AgentDetailsController
    {
        AgentDetails newagentID = new AgentDetails();
        

        public Guid GetAgent(string compname, string bionSN)
        {
           
            try
                {
                
                    // Read a particular key from the config file ;
                    var url = ConfigurationManager.AppSettings["apiems"];

                    //post agent/computer name to the database

                    var client = new RestClient(url);

                    var softrequest = new RestRequest("v1/AgentDevices/" + compname + "/" + bionSN, Method.GET);

                    IRestResponse softresponse = client.Execute(softrequest);

                if (softresponse.StatusCode != HttpStatusCode.OK)
                {

                    WriteLog.Write(string.Format("{0} Returned, Web service is down \n",
                      url, softresponse.StatusDescription));
                                     
                }

                return Guid.Parse(softresponse.Content.Replace("/", "").Replace("\"", ""));
                

                   // return Guid.Parse(softresponse.Content.Replace("/", "").Replace("\"", ""));
                
                }
                
                catch (Exception e)
                {
                    e.Message.ToString();
                    WriteLog.Write("ERROR: " + e.Message.ToString());
                    return Guid.Empty;
                }
        


        }

    }
}
