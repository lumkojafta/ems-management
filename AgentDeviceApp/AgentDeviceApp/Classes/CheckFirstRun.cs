﻿using System;
using System.Configuration;

namespace AgentDeviceApp.Classes
{
    class CheckFirstRun
    {
        //Method to run all the agents when the system is first run 
        public void FirstRun() 
        {
            try
            {
                //Properties.Resources.firstrun;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string serialNumber = string.Empty;
                string compname = System.Environment.MachineName;             

                    if (ConfigurationManager.AppSettings["firstrun"] != null)
                    //Properties.Resources.firstrun;
                    {
                        if (config.AppSettings.Settings["firstrun"].Value != "true")
                        {
                            // Config.GetConfig().Value1;
                            //string oldValue = config.AppSettings.Settings["firstrun"].Value;
                            config.AppSettings.Settings["firstrun"].Value = "true";
                            config.Save(ConfigurationSaveMode.Modified);

                            WriteLog.Write("The agent has been scheduled to run...\n");
                            MyAgentCalls agent = new MyAgentCalls();
                            agent.AgentCalls();
                        }
                    }
                    else
                    {
                        config.AppSettings.Settings.Add("firstrun", "false");
                        config.Save(ConfigurationSaveMode.Modified);
                        ConfigurationManager.RefreshSection("appSettings");

                    }
                
                Console.ReadKey();
                
            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }


        }
    }
   
}

