﻿using RestSharp;
using System;
using System.Configuration;
using System.Management;

namespace AgentDeviceApp.Classes
{

    class HardwareController
    {
        //Getting Hardware of the agent from Win32 platform
        HardwareDetails p = new HardwareDetails();

        public Guid GetHardware(Guid Agentuuid)
        {

            try
            {

                WriteLog.Write("Info: Agent application has started running Hardware details \n");

                ObjectQuery namequery = new ObjectQuery("SELECT Name FROM Win32_ComputerSystem");
                ManagementObjectSearcher namesearcher = new ManagementObjectSearcher(namequery);

                ObjectQuery sysquery = new ObjectQuery("SELECT * FROM Win32_Product");
                ManagementObjectSearcher syssearcher = new ManagementObjectSearcher(sysquery);

                ObjectQuery OperatingSys = new ObjectQuery("select * from Win32_OperatingSystem");
                ManagementObjectSearcher ossearcher = new ManagementObjectSearcher(OperatingSys);

                ObjectQuery memory = new ObjectQuery("select * from Win32_PhysicalMemory");
                ManagementObjectSearcher memsearcher = new ManagementObjectSearcher(memory);

                ObjectQuery Procesorquery = new ObjectQuery("Select * from Win32_Processor");
                ManagementObjectSearcher prosearcher = new ManagementObjectSearcher(Procesorquery);

                ObjectQuery biosquery = new ObjectQuery("SELECT * FROM Win32_Bios");
                ManagementObjectSearcher biosearcher = new ManagementObjectSearcher(biosquery);

                ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_ComputerSystem");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);

                ObjectQuery IPquery = new ObjectQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE");
                ManagementObjectSearcher IPsearcher = new ManagementObjectSearcher(IPquery);


                foreach (ManagementObject managementObject in syssearcher.Get())
                {

                    if (managementObject["InstallDate"] != null)
                    {

                        p.OSInstallDate = DateTime.ParseExact(managementObject["InstallDate"].ToString(), @"yyyyMd", System.Globalization.CultureInfo.InvariantCulture);
                        p.LastCollectedDate = DateTime.Now;

                    }
                }

                foreach (ManagementObject managementObject in searcher.Get())
                {

                    p.UserName = managementObject["UserName"] != null ? managementObject["UserName"].ToString() : null;
                    p.DomainName = managementObject["Domain"] != null ? managementObject["Domain"].ToString() : null;


                }
                foreach (ManagementObject managementObject in namesearcher.Get())
                {


                    p.ComputerName = managementObject["Name"] != null ? managementObject["Name"].ToString() : null;

                }

                foreach (ManagementObject managementObject in biosearcher.Get())
                {

                    p.BIOSName = managementObject["Name"] != null ? managementObject["Name"].ToString() : null;
                    p.BIOSManufacturer = managementObject["Manufacturer"] != null ? managementObject["Manufacturer"].ToString() : null;
                    p.BIOSVersion = managementObject["SMBIOSBIOSVersion"] != null ? managementObject["SMBIOSBIOSVersion"].ToString() : null;
                    p.BiosSN = managementObject["SerialNumber"] != null ? managementObject["SerialNumber"].ToString() : null;
                    p.OSMajorVersion = managementObject["SystemBiosMajorVersion"] != null ? managementObject["SystemBiosMajorVersion"].ToString() : null;
                    p.OSMinorVersion = managementObject["SystemBiosMinorVersion"] != null ? managementObject["SystemBiosMinorVersion"].ToString() : null;

                }

                foreach (ManagementObject managementObject in prosearcher.Get())
                {

                    p.ProcessorName = managementObject["Name"] != null ? managementObject["Name"].ToString() : null;
                    p.ProcessorCores = managementObject["NumberOfCores"] != null ? managementObject["NumberOfCores"].ToString() : null;
                    p.ProcessorClockFrequency = managementObject["CurrentClockSpeed"] != null ? managementObject["CurrentClockSpeed"].ToString() : null;

                }
                foreach (ManagementObject managementObject in ossearcher.Get())
                {

                    p.OperatingSystem = managementObject["Caption"] != null ? managementObject["Caption"].ToString() : null;

                }
                foreach (ManagementObject managementObject in memsearcher.Get())
                {

                    double dblMemory;

                    if (double.TryParse(Convert.ToString(managementObject["Capacity"]), out dblMemory))
                    {

                        dblMemory = Convert.ToInt32(dblMemory / (1024 * 1024 * 1024));

                        p.Memory = dblMemory.ToString();

                    }
                }

                foreach (ManagementObject managementObject in IPsearcher.Get())
                {
                    if (managementObject["Description"] != null)
                    {
                        p.Description = managementObject["Description"].ToString();   //Display operating system caption

                    }

                    string[] addresses = (string[])managementObject["IPAddress"];

                    foreach (string ipaddress in addresses)
                    {
                        p.IPAddress = ipaddress;
                    }
                }

                p.AgentDeviceuuid = Agentuuid;

                //hardware post
                var url = ConfigurationManager.AppSettings["apiems"];

                var client = new RestClient(url);

                var request = new RestRequest("/v1/HardwareDetails/CreateAgent", Method.POST);

                request.AddJsonBody(p); // adds to POST or URL querystring based on Method



                IRestResponse<HardwareDetails> response2 = client.Execute<HardwareDetails>(request);

                syssearcher.Dispose();
                ossearcher.Dispose();
                prosearcher.Dispose();
                biosearcher.Dispose();
                searcher.Dispose();
                IPsearcher.Dispose();

                return response2.Data.UUID;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
                return Guid.Empty;
            }
        }
    }
}
