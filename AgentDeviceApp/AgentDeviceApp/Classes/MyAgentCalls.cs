﻿using System;
using System.Management;

namespace AgentDeviceApp.Classes
{
    class MyAgentCalls
    {
        //Method that calls all the Agent methods
        public void AgentCalls()
        {
            string serialNumber = string.Empty;

            ManagementObjectSearcher MOS = new ManagementObjectSearcher(" Select * From Win32_BIOS ");
            foreach (ManagementObject getserial in MOS.Get())
            {
                serialNumber = getserial["SerialNumber"].ToString();
            }

            string compname = System.Environment.MachineName;
            
            AgentDetailsController ag = new AgentDetailsController();
           
            Guid IDD = ag.GetAgent(compname, serialNumber);
           

            //    HardwareController hardcont = new HardwareController();
            //    var uuid = hardcont.GetHardware(IDD);

            //IPAddressController ipccont = new IPAddressController();
            //ipccont.GetIPAddress(uuid);

            //DiskController diskcont = new DiskController();
            //diskcont.GetDisk(IDD);

            //SoftwareController softcont = new SoftwareController();
            //softcont.GetSoftawre(IDD);

            RegistrymanagementController ipcont = new RegistrymanagementController();

            ipcont.GetMangementRegistry(IDD);


            WriteLog.Write("Agent application has completed running...\n");
            }


        
    }
}
