﻿using RestSharp;
using System;
using System.Configuration;
using System.Net;
using System.Threading;

namespace AgentDeviceApp.Classes
{
    class MySchedulerController
    {
        //Method to run the agent on demand 

        public void Schedule(Guid agentuuid) //Get All Events Records  
        {
            try
            {               
                do
                {
                    //softwar post
                    var url = ConfigurationManager.AppSettings["apiems"];

                    // adds to GET or URL querystring based on Method
                    var client = new RestClient(url);

                    var softrequest = new RestRequest("/v1/Myscheduler/" + agentuuid, Method.GET);

                    IRestResponse softresponse = client.Execute(softrequest);


                    if (softresponse.StatusCode == HttpStatusCode.OK)
                    {
                        var softcontent = softresponse.Content; // raw content as string
                        if (softcontent != "false")
                        {
                            WriteLog.Write("The agent has been scheduled to run on demand...\n");
                            MyAgentCalls agent = new MyAgentCalls();
                            agent.AgentCalls();
                        }
                    }

                    Thread.Sleep(001 * 1000);

                } while (1 == 1);

            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }


        }


    }
}
