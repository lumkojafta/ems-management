﻿using AgentDeviceApp.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AgentDeviceApp.Classes
{
    class RegistryController
    {
       

        public RegistryDetails GetRegistryLocalMachine(string location, Guid Agentuuid, string keytype, string keynamee)
        {            
            try
            {
                //Getting Registry Details and value keys of the agent

                List<RegistryDetails> installedPrograms = new List<RegistryDetails>();
                RegistryHive hive = new RegistryHive();              

                RegistryDetails newreg = new RegistryDetails();

                if (keytype == "RegistryHive.LocalMachine")
                {
                    hive = RegistryHive.LocalMachine;
                }
                if (keytype == "RegistryHive.CurrentUser")
                {
                    hive = RegistryHive.CurrentUser;
                }
               
                using (var regHive = RegistryKey.OpenBaseKey(hive, RegistryView.Registry64))
                using (var regLocation = regHive.OpenSubKey(location, RegistryKeyPermissionCheck.ReadSubTree))
                {
                    if (regLocation != null)
                    {
                        //.GetValueNames())
                    
                        var keyValue = regLocation.GetValue(keynamee) != null ? newreg.Value = regLocation.GetValue(keynamee).ToString() : "Not Found";
                                                                                    
                        newreg.Key = keynamee.ToString();
                        newreg.Value = keyValue.ToString();
                        newreg.Hive = location;
                        installedPrograms.Add(newreg);

                    }
                    else
                    {
                        newreg.Key = keynamee.ToString();
                  
                        newreg.Value = "Not found";
                        newreg.Hive = location;
                        installedPrograms.Add(newreg);
                    }
                    
                }               

                newreg.AgentDeviceuuid = Agentuuid;
                               

                return newreg;
            }
            
            catch (UnauthorizedAccessException ex)
            {
                ex.Message.ToString();
                WriteLog.Write("ERROR: " + ex.Message.ToString());
                return null;
            }

            }

       
    }
}
