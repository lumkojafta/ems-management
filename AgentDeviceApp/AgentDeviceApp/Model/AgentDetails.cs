﻿using System;

namespace EMS.Models
{
    public class AgentDetails
    {
        public Guid UUID { get; set; }

        public string ComputerName { get; set; }

        public string BiosSN { get; set; }
    }

}