﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentDeviceApp.Model
{
    class RegistryDetails
    {
        public Guid UUID { get; set; }
        public Guid AgentDeviceuuid { get; set; }        
        public string Hive { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
        public DateTime Lastpolled { get; set; }
        public string KeyName { get; set; }
        public List<System.Collections.Generic.KeyValuePair<string, object>> Values { get; set; }

    }
}
