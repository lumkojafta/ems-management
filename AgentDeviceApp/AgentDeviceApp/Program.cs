using AgentDeviceApp.Classes;
using System;
using System.IO;
using System.Management;
using System.Threading;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        { 
          
                 
                //Creates the Log file 
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    WriteLog.Write("Agent application has started running /n");
                 
                }                  
                
                string serialNumber = string.Empty;

                ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * From Win32_BIOS ");
                foreach (ManagementObject getserial in MOS.Get())
                {
                    serialNumber = getserial["SerialNumber"].ToString();
                }

                string compname = System.Environment.MachineName;

                AgentDetailsController ag = new AgentDetailsController();
                Guid IDD = ag.GetAgent(compname, serialNumber);
                if (IDD != Guid.Empty)
                {

                MyAgentCalls my = new MyAgentCalls();
                MySchedulerController sch = new MySchedulerController();

                //calls the maethod that first run then all the agents
                CheckFirstRun fr = new CheckFirstRun();
                fr.FirstRun();


                //calls the method that schedule the agents on demand
                Thread tid2 = new Thread(() => sch.Schedule(IDD));

                tid2.Start();
                }
            
        }

    }
}






    

    




