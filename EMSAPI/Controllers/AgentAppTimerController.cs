using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMS.Models;
using EMS.Models.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace EMS.Controllers
{

    [Route("v1/[controller]")]
    [ApiController]
    public class AgentAppTimerController : Controller
    {
        private readonly EMSDBContext _context;
        //EMSDBContext _context = new EMSDBContext();
        public AgentAppTimerController(EMSDBContext context)
        {
            _context = context;

        }      

        /// <summary>
        /// List all Scheduler (Devices)
        /// </summary>
        // GET api/values/5
        [HttpGet]
         [Route("CreateTimer")]
         
         //Method to check if is the end of the month 
        public  IActionResult CreateTimer()          
        {

           try {  
                            
                  DateTime olddate =  DateTime.Now;
                 var today = olddate.Date;
                 
                var monthend = LastDayOfMonthFromDateTime(today);
                int num = 0;

                switch (DateTime.Now.Hour)
                {
                    case 20: 
                     num = 1;
                        break;
                    case 21:    
                     num = 2;
                        break;
                    case 22: 
                     num = 3;
                        break;
                    case 23: 
                     num = 4;
                        break;
                    case 00: 
                     num = 5;
                        break;
                } 
                  
                var number = _context.AgentDevices.Where(x => x.AgentCouter == num).ToList();              

                if(monthend == today)
                {            
    
                if (number.Any() )
                {
                    // Mapping
                    foreach (var re in number)
                    {
                    Myscheduler SchDetails = new Myscheduler()
                    {
                    
                        AgentDeviceuuid = re.UUID

                    };                 

                    _context.Myscheduler.Add(SchDetails);
                    }

                    _context.SaveChanges();
                    }
                }
             return Ok();
               
            }
            catch (Exception error) {
             
                return BadRequest(error);
            }
       
        }
        //Get the last day of the month
         public static DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
        DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
        return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }  
    }
}