using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMS.Models;
using EMS.Models.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace EMS.Controllers
{

    [Route("v1/[controller]")]
    [ApiController]
    public class MyschedulerController : Controller
    {
        private readonly EMSDBContext _context;
        //EMSDBContext _context = new EMSDBContext();
        public MyschedulerController(EMSDBContext context)
        {
            _context = context;

        }
        /// <summary>
        /// Create a schedular
        /// </summary>
        [HttpPost]
        [Route("{uuid}")]
        public IActionResult Schedule(Guid uuid)
        {
            // var agantid =  uuid;
            var agantid = _context.Myscheduler.Where(x => x.AgentDeviceuuid == uuid).FirstOrDefault();
            if (agantid == null)
            {

                // Mapping
                Myscheduler SchDetails = new Myscheduler()
                {
                    UUID = Guid.NewGuid(),
                    AgentDeviceuuid = uuid

                };

                _context.Myscheduler.Add(SchDetails);

                _context.SaveChanges();
            }

            return Ok();
        }

        /// <summary>
        /// List all Scheduler (Devices)
        /// </summary>
        // GET api/values/5
        [HttpGet]
        [Route("{uuid}")]
        public async Task<IActionResult> Scheduler(Guid uuid)
        {

            try
            {

                var results = await _context.Myscheduler.Where(c => c.AgentDeviceuuid == uuid).ToListAsync();
                if (results.Any())
                {


                    //results remove after deleting
                    foreach (var re in results)
                    {
                        _context.Myscheduler.Remove(re);
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Ok(false);
                }

            }
            catch (Exception error)
            {

                return BadRequest(error);
            }
        }

    }
}