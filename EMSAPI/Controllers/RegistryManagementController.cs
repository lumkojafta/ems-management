using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMS.Models;
using EMS.Models.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace EMS.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
public class RegistryManagementController : Controller
    {

        private readonly EMSDBContext _context;
         //EMSDBContext _context = new EMSDBContext();
         public RegistryManagementController(EMSDBContext context)
        {
            _context = context;
            
        }

       /// <summary>
        /// List all RegistryValues
        /// </summary>

        [HttpGet]
        [Route("GetRegistryValues")]
        public async Task<IActionResult> GetRegistryValues()
        {
           try{
            return Ok(await _context.RegistryManagement.ToListAsync());
        }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return StatusCode(500, ex);
            }
        }
      
        // Save a registry key
        [HttpPost]
         public IActionResult PostReg([FromBody] RegistryManagement regValues) 
        {     

            try
            {
            RegistryManagement Existing = _context.RegistryManagement.Find(regValues.UUID);

                  if (Existing == null)
                {          
                 // Mapping              
                  
                    RegistryManagement reggman = new RegistryManagement()
                    {                   
                        UUID = Guid.NewGuid(),    
                        Key = regValues.Key, 
                        Type = regValues.Type,
                        Location = regValues.Location,
                        DateCreated = DateTime.Now,
                        DateModified = DateTime.Now,                                             
                      
                    };
                     _context.RegistryManagement.Add(reggman);    
                }
                else{
                        Existing.Key = regValues.Key;
                        Existing.Type = regValues.Type;
                        Existing.Location = regValues.Location;                 
                        Existing.DateModified = DateTime.Now;                    
                      
                        _context.RegistryManagement.Update(Existing);

                }           
                                          
               _context.SaveChanges();
            }
                 catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }    
                   return Ok();        
        }
            
        // DELETE a Registry key
        [HttpDelete("{uuid}")]
       
        public IActionResult DeleteReg(Guid uuid)  
              {        
             try
            {
                 var obj =  _context.RegistryManagement.Find(uuid);
               //var obj = _context.RegistryManagement.Where(x => x.UUID == uuid).First();
                if (obj != null)
                {
                    _context.RegistryManagement.Remove(obj);
                     _context.SaveChanges();
                    return Ok(true);
                }
                else return Ok(false);                
              
            }
            catch (Exception error)
            {   
                return BadRequest(error);
            }

        }
    }
}

 