using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EMS.Models;
using EMS.Models.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace EMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
public class SoftwareDetailsController : Controller
    {
        private readonly EMSDBContext _context;
   
         public SoftwareDetailsController(EMSDBContext context)
        {
            _context = context;            
        }

        /// <summary>
     /// List of Software of the specific agent
        /// </summary>    

        [HttpGet]
        [Route("{uuid}")]
        public async Task<IActionResult> GetSoftwareUUID(Guid uuid)
        {
              
           try {
                var results = await _context.SoftwareDetails.Where(c => c.AgentDeviceuuid == uuid).ToListAsync();                
                return Ok(results);
            }
            catch (Exception error) {
             
                return BadRequest(error);
            }
        }

        /// <summary>
        /// List all sofwaredetails
        /// </summary>

        [HttpGet]
        [Route("GetSoftwareInfo")]
        public async Task<IActionResult> GetSoftwareDetails()
        {
           try{
            return Ok(await _context.SoftwareDetails.ToListAsync());
        }
              catch (Exception ex)
            {
                ex.Message.ToString();
                return StatusCode(500, ex);
            }
        }
        
         [HttpPost]
         [Route("CreateSoftWare")]
        public async Task<IActionResult> CreateSoftware([FromBody] List<SoftwareDetails> soft)
        {       
                 try {

                     var agantuuid=   soft[0].AgentDeviceuuid;
                     if(agantuuid != null)
                     {
                     var results =  _context.SoftwareDetails.Where(c => c.AgentDeviceuuid == agantuuid);

                      foreach(var re in results)
                      {
                             _context.SoftwareDetails.Remove(re);
                      }
                    }
                foreach( var softt in soft)
                {
               
                SoftwareDetails softdetails= new SoftwareDetails()
                {                   
                    UUID = Guid.NewGuid(),   
                    AgentDeviceuuid = softt.AgentDeviceuuid,  
                    Name = softt.Name,
                    Version = softt.Version,
                    InstallLocation =softt.InstallLocation,
                    OSInstallDate = softt.OSInstallDate,
                    Publisher= softt.Publisher,
                    Lastpolled = DateTime.Now             
                };
                
                _context.SoftwareDetails.Add(softdetails); 
              
                               
                }
              

              await _context.SaveChangesAsync();
            }
            catch (Exception error) {
             
                return BadRequest(error);
            }
            

              return Ok();
        }
        
    }
}