﻿using System;
using System.Configuration;

namespace AgentDeviceApp.Classes
{
    class CheckFirstRun
    {
        //Method to run all the agents when the system is first run 
        public void FirstRun() 
        {
            try
            {
                // setting the config file to default:false
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (ConfigurationManager.AppSettings["firstrun"] != null)
                 
                {
                    if (config.AppSettings.Settings["firstrun"].Value != "true")
                    {
                     
                        
                        config.AppSettings.Settings["firstrun"].Value = "true";
                        config.Save(ConfigurationSaveMode.Modified);

                        WriteLog.Write("The agent has been scheduled to run...\n");
                        MyAgentCalls agent = new MyAgentCalls();
                        agent.AgentCalls();
                    }
                }
                else
                {
                    config.AppSettings.Settings.Add("firstrun", "false");
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                }
            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }


        }
    }
   
}

