﻿using EMS.Models;
using RestSharp;
using System;
using System.Configuration;
using System.IO;
using System.Management;

namespace AgentDeviceApp.Classes
{
    class DiskController
    {
        //Getting Disk info of the agent from DriveInfo platform
        public void GetDisk(Guid Agentuuid)
        {
            try
            {
                 WriteLog.Write("Info: Agent application has started running Disks\n");               
                 //Get the required disk info from the Win32 platform
                DriveInfo[] allDrives = DriveInfo.GetDrives();                
                ObjectQuery sysquery = new ObjectQuery("SELECT * FROM Win32_Product");
                ManagementObjectSearcher syssearcher = new ManagementObjectSearcher(sysquery);

                foreach (DriveInfo d in allDrives)
                {
                    DiskDetails newdisk = new DiskDetails();

                    if (d.IsReady == true && d.DriveType == DriveType.Fixed)
                    {
                        newdisk.Name = d.Name.ToString();

                        newdisk.Drive = d.DriveType.ToString();

                        newdisk.DriveFormat = d.DriveFormat.ToString();


                        if (d.TotalSize > 1073741824) //check the drive is MB or GB
                        {
                            long ans = (d.TotalSize / 1073741824) - (d.AvailableFreeSpace / 1073741824);

                            newdisk.UsedSpace = ans.ToString();
                        }

                        if (d.AvailableFreeSpace > 1073741824)
                        {
                            long ans = (d.AvailableFreeSpace / 1073741824);

                            newdisk.AvailableFreeSpace = ans.ToString();
                        }
                        if (d.TotalSize > 1073741824)
                        {
                            long ans = (d.TotalSize / 1073741824);

                            newdisk.TotalSize = ans.ToString();

                        }

                        newdisk.AgentDeviceuuid = Agentuuid;

                        //post disk info  to the database

                        var url = ConfigurationManager.AppSettings["apiems"];

                        var client = new RestClient(url);                       

                        var diskequest = new RestRequest("v1/DiskDetails/CreateDisk", Method.POST);

                        diskequest.AddJsonBody(newdisk); // adds to POST or URL querystring based on Method


                        IRestResponse diskresponse = client.Execute(diskequest);
                        var diskcontent = diskresponse.Content;
                       
                    }
                }                
            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }
        }
    }
}
