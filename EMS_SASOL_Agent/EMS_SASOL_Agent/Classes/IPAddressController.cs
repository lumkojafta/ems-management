﻿using AgentDeviceApp.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Management;

namespace AgentDeviceApp.Classes
{
    class IPAddressController
    {
        //Getting IP address of the agent from Win32 platform

        public void GetIPAddress(Guid Agentuuid)
        {
            try
            {
                
                WriteLog.Write("Info: Agent application has started running IP Address \n");

                 //Get the required ip address info from the Win32 platform
                ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE AND DHCPEnabled = TRUE");

                List<IPAddressDetails> installedPrograms = new List<IPAddressDetails>();              

                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);

                foreach (ManagementObject managementObject in searcher.Get())
                {
                    string sIPAddress = searcher.Get().Cast<ManagementObject>().SelectMany(o => (string[])(o["IPAddress"]))
                    .FirstOrDefault(a => a.Contains('.'));

                    string[] addresses = (string[])managementObject["IPAddress"];

                    foreach (string ipaddress in addresses)
                    {
                        IPAddressDetails ipadd = new IPAddressDetails();
                        ipadd.Description = managementObject["Description"] != null ? managementObject["Description"].ToString() : "Not Found";
                                                
                        ipadd.IPAddress = sIPAddress.ToString();

                        ipadd.AgentDeviceuuid = Agentuuid;                     
                       
                        installedPrograms.Add(ipadd);
                    }
                   
                }

                //post ip addresses info  to the database
                var url = ConfigurationManager.AppSettings["apiems"];
                var client = new RestClient(url);
                var softrequest = new RestRequest("v1/IPAddressDetails/CreateIPAddress", Method.POST);

                softrequest.AddJsonBody(installedPrograms); // adds to POST or URL querystring based on Method

                IRestResponse softresponse = client.Execute(softrequest);
                var softcontent = softresponse.Content; // raw content as string

            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }
        }

    }
}
