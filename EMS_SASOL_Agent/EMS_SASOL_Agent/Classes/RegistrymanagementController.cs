﻿using AgentDeviceApp.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace AgentDeviceApp.Classes
{
    class RegistrymanagementController
    {
        public void GetMangementRegistry(Guid uuid)
        {
            try
            {
                WriteLog.Write("Info: Agent application has started running Registry Keys \n");
                List<Registrymanagement> reglist = new List<Registrymanagement>();
                RegistryController regc = new RegistryController();
                RegistryDetails newreg = new RegistryDetails();
                List<RegistryDetails> regglist = new List<RegistryDetails>();
                //softwar post
                var url = ConfigurationManager.AppSettings["apiems"];
                var client = new RestClient(url);

                var softrequest = new RestRequest("/v1/RegistryManagement/GetRegistryValues", Method.GET);

                IRestResponse<List<Registrymanagement>> response2 = client.Execute<List<Registrymanagement>>(softrequest);

                //list.GetRegistryLocalMachine.ForEach(x => { response2.Data.uuid(x.Name); });            
                var values = response2.Data;

                foreach (var name in values)
                {

                    regglist.Add(regc.GetRegistryLocalMachine(name.Location, uuid, name.Type, name.Key));

                }

                var newclient = new RestClient(url);

                var softerrequest = new RestRequest("v1/RegistryDetails/CreateRegistryValues", Method.POST);

                softerrequest.AddJsonBody(regglist); // adds to POST or URL querystring based on Method

                IRestResponse softresponse = client.Execute(softerrequest);
                var softcontent = softresponse.Content; // raw content as string

            }
            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }

        }
    }
}
