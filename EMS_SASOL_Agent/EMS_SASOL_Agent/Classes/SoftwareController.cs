﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Management;

namespace AgentDeviceApp.Classes
{
    class SoftwareController
    {
        public void GetSoftawre(Guid Agentuuid)
        {
            try
            {
                //Getting software of the agent from Win32 platform

                WriteLog.Write("Info: Agent application has started running Software \n");

                List<SoftwareDetails> installedPrograms = new List<SoftwareDetails>();

                ObjectQuery sysquery = new ObjectQuery("SELECT * FROM Win32_Product");
                ManagementObjectSearcher syssearcher = new ManagementObjectSearcher(sysquery);

                foreach (ManagementObject managementObject in syssearcher.Get())
                {
                    SoftwareDetails s = new SoftwareDetails();

                    s.Name = managementObject["Caption"] != null ? managementObject["Caption"].ToString() : "Not Found";

                    s.Version = managementObject["Version"] != null ? managementObject["Version"].ToString() : "Not Found";

                    s.InstallLocation = managementObject["InstallLocation"] != null ? managementObject["InstallLocation"].ToString() : "Not Found";

                    s.OSInstallDate = managementObject["InstallDate"] != null ? DateTime.ParseExact(managementObject["InstallDate"].ToString(), @"yyyyMd", System.Globalization.CultureInfo.InvariantCulture) : new DateTime();
                   
                    s.Publisher = managementObject["Vendor"] != null ? managementObject["Vendor"].ToString() : "Not Found";                   

                    s.AgentDeviceuuid = Agentuuid;

                    installedPrograms.Add(s);
                }

                //softwar post
                var url = ConfigurationManager.AppSettings["apiems"];
                var client = new RestClient(url);
                var softrequest = new RestRequest("/api/SoftwareDetails/CreateSoftWare", Method.POST);

                softrequest.AddJsonBody(installedPrograms); // adds to POST or URL querystring based on Method

                IRestResponse softresponse = client.Execute(softrequest);
                var softcontent = softresponse.Content; // raw content as string
            }

            catch (Exception e)
            {
                e.Message.ToString();
                WriteLog.Write("ERROR: " + e.Message.ToString());
            }
        }
    }
}
