﻿using System;

namespace AgentDeviceApp.Model
{
    class CheckFirstRun
    {
        public Guid UUID { get; set; }
        public Guid AgentDeviceuuid { get; set; }
    }
}
