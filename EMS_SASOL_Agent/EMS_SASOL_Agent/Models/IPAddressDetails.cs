﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentDeviceApp.Model
{
    class IPAddressDetails
    {
      
            public Guid UUID { get; set; }
            public Guid AgentDeviceuuid { get; set; }          
            public string Description { get; set; }           
            public string IPAddress { get; set; }
       
    }
}
