﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentDeviceApp.Model
{
    class Registrymanagement
    {
        public Guid UUID { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
