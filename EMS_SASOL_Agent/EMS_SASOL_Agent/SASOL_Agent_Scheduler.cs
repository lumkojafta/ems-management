﻿using AgentDeviceApp.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace EMS_SASOL_Agent
{
    public partial class SASOL_Agent_Scheduler : ServiceBase
    {
        private System.Timers.Timer timer1;

        public SASOL_Agent_Scheduler()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new System.Timers.Timer();
            this.timer1.Interval = 30000; //every 30 secs           
            timer1.Enabled = true;
           
            //Creates the Log file 
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                WriteLog.Write("Agent application has started running /n");

            }

            string serialNumber = string.Empty;

            ManagementObjectSearcher MOS = new ManagementObjectSearcher(" Select * From Win32_BIOS ");
            foreach (ManagementObject getserial in MOS.Get())
            {
                serialNumber = getserial["SerialNumber"].ToString();
            }

            string compname = System.Environment.MachineName;

            AgentDetailsController ag = new AgentDetailsController();
            Guid IDD = ag.GetAgent(compname, serialNumber);
            if (IDD != Guid.Empty)
            {

                MyAgentCalls my = new MyAgentCalls();
                MySchedulerController sch = new MySchedulerController();

                //calls the maethod that first run then all the agents
                CheckFirstRun fr = new CheckFirstRun();
                fr.FirstRun();

                //calls the method that schedule the agents on demand
                Thread tid2 = new System.Threading.Thread(() => sch.Schedule(IDD));
                tid2.Start();
            }

        }

        protected override void OnStop()
        {
            timer1.Enabled = false;
           WriteLog.Write(" Agent service has been stopped");
        }
      
    }
}
