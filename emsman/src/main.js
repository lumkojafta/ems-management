
import 'babel-polyfill';
Vue.use(require('babel-polyfill'));

import Vue from 'vue'
import App from './App.vue'

import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'





// Import Moments JS for DateTime Stuff
Vue.use(require('vue-moment'));
import moment from "moment";
Vue.filter("formatDate", function (value) {
  if (value) {``
    return moment(String(value)).format("DD/MM/YYYY HH:mm:ss");
  }
});

Vue.filter("formatDateName", function (value) {
  if (value) {
    return moment(String(value)).format("Do MMMM YYYY, HH:mm:ss");
  }
});

// Import Config File
import config from "@/config.json";


try {
  var configlocal = require("@/config_local.json");
  // do stuff
} catch (ex) {
  null;
}
if (configlocal == null) {
  if (process.env.NODE_ENV == "production") {
    Vue.prototype.$conf = config.production;
    store.commit("setEnviroment", "production");
  } else {
    if (process.env.NODE_ENV == "qa") {
      Vue.prototype.$conf = config.qa;
      store.commit("setEnviroment", "qa");
    } else {
      if (process.env.NODE_ENV == "dev") {
        Vue.prototype.$conf = config.dev;
        store.commit("setEnviroment", "dev");
      } else {
        Vue.prototype.$conf = config.development;
        store.commit("setEnviroment", "development");
      }
    }
  }
} else {
  Vue.prototype.$conf = configlocal.development;
  store.commit("setEnviroment", "development");
}

import Sortable from 'vue-sortable'

Vue.use(Sortable)

Vue.use(VueResource);


//import vuetify from './plugins/vuetify';

Vue.use(Vuetify, {
  theme: {
    primary: "#000000",
    secondary: "#ffffff",
    accent: "#e21e2a"
  },
  iconfont: 'md',
})

Vue.config.productionTip = false

import "./importEMSComponents";
// Import Config File
//import config from "@/config.json";

new Vue({
  Vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
