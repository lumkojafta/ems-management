import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/devicedetails",
    name: "devicedetails",
    //component: () => import("@/views/devicedetails.vue")
    component: () => import(/* webpackChunkName: "about" */ '../views/devicedetails.vue')
  },
  {
    path: "/RegistryKeyManagement",
    name: "RegistryKeyManager",
    component: () => import(/* webpackChunkName: "about" */ '../views/RegistryKeyManager.vue')
  },
]

const router = new VueRouter({
  mode: "history",
  routes
})

export default router
