import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {  
    editedDevice: null,
    loading: false,
    AgentUUID: null,
  },

  mutations: {
    updateEditedDevice(state, dev) {
      state.editedDevice = dev;
    },
    updateAgentuuid(state, AgentDevUUID) {
      state.AgentUUID = AgentDevUUID;
    },
    updateloading(state, showhide) {
      state.loading = showhide;
    },
    clearstore(state) {
      state.loading = true;
    }
  },

  actions: {
  },

  modules: {
  }
})
