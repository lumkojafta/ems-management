const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
  productionSourceMap: false,
  transpileDependencies: ["vuetify"],
  configureWebpack: {
    plugins: [new CompressionPlugin()],
    entry: ["babel-polyfill", "./src/main.js"]
  }
};

